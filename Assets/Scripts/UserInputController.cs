﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInputController : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    [SerializeField] private float dragSpeed = 30;
    
    private Vector3 lastMousePosition;
    private Vector3 mouseMovement;

    private void Update()
    {
        mouseMovement = Input.mousePosition - lastMousePosition;
        ProceedMouseInput();
        lastMousePosition = Input.mousePosition;
    }

    private void ProceedMouseInput()
    {
        if (Input.GetMouseButton(0))
        {
            DragObject();
        }
    }

    private void DragObject()
    {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray);

        if (hit.rigidbody == null)
        {
            return;
        }
        
        if (!hit.rigidbody.isKinematic)
        {
            hit.rigidbody.transform.position += mainCamera.ScreenToViewportPoint(mouseMovement) * dragSpeed;
        }
    }
}
